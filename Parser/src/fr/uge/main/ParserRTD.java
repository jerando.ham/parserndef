package fr.uge.main;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ParserRTD {

	private static String hexToAscii(String hexStr) {
	    StringBuilder output = new StringBuilder("");
	    
	    for (int i = 0; i < hexStr.length(); i += 2) {
	        String str = hexStr.substring(i, i + 2);
	        output.append((char) Integer.parseInt(str, 16));
	    }
	    
	    return output.toString();
	}
	
	private static String hexToBin(String hexStr) {
		//String str = "1F"; // notre chaine hexa
		int i = Integer.parseInt(hexStr, 16); // on demande une conversion en entier; on indique que l'on utilise une base 16
		return Integer.toBinaryString(i) ; // on affiche la représentation binaire
	}
	
	private static String hexToDec(String hexStr) {
		//String str = "1F"; // notre chaine hexa
		int i = Integer.parseInt(hexStr, 16); // on demande une conversion en entier; on indique que l'on utilise une base 16
		return Integer.toString(i) ; // on affiche la représentation binaire
	}
	
	private static List<String> getParts(String string, int partitionSize) {
	    List<String> parts = new ArrayList<String>();
	    int len = string.length();
	    for (int i = 0; i < len; i += partitionSize) {
	      parts.add(string.substring(i, Math.min(len, i + partitionSize)));
	    }
	    return parts;
	  }
	
	
	public static void main(String[] args) {
		
		Map<String,String> mapURI= new HashMap<String, String>();
		
		mapURI.put("00", "NA");
		mapURI.put("01", "http://www.");
		mapURI.put("02", "https://www.");
		mapURI.put("03", "http://");
		mapURI.put("04", "https://");
		mapURI.put("05", "tel:");
		mapURI.put("06", "mailto:");
		mapURI.put("07", "ftp://anonymous:anonymous@");
		mapURI.put("08", "ftp://ftp.");
		mapURI.put("09", "ftps://");
		//String message = "D1 01 57 55 04 69 67 6D 2E 75 2D 70 65 6D 2E 6672 2F 66 6F 72 6D 61 74 69 6F 6E 73 2F 6D 61 7374 65 72 2D 32 2D 73 79 73 74 65 6D 65 73 2D 6574 2D 73 65 72 76 69 63 65 73 2D 70 6F 75 72 2D6C 2D 69 6E 74 65 72 6E 65 74 2D 64 65 73 2D 6F62 6A 65 74 73 2D 73 73 69 6F 2F";
		
		String message = "D1 01 57 55 04 69 67 6D 2E 75 2D 70 65 6D 2E 66 72 2F 66 6F 72 6D 61 74 69 6F 6E 73 2F 6D 61 73 74 65 72 2D 32 2D 73 79 73 74 65 6D 65 73 2D 65 74 2D 73 65 72 76 69 63 65 73 2D 70 6F 75 72 2D 6C 2D 69 6E 74 65 72 6E 65 74 2D 64 65 73 2D 6F 62 6A 65 74 73 2D 73 73 69 6F 2F";
		
		
		//System.out.println(message);
		List<String> listHexa = new ArrayList<String>();
		listHexa = getParts(message, 3);
		//System.out.println(listHexa);
		int messageLength = 0;
		
		
		for(int i=0; i<listHexa.size();i++) {
			if(i==0) {
				System.out.println( "Header : " +hexToBin(listHexa.get(i).trim()));
			}
			if(i==1) {
				System.out.println("record name length : " + hexToDec(listHexa.get(i).trim()));
			}
			if(i==2) {
				System.out.println("message length : " + hexToDec(listHexa.get(i).trim()));	
				messageLength = Integer.parseInt(hexToDec(listHexa.get(i).trim()));
			}
			if(i==3) {
				System.out.println("record type : " + hexToAscii(listHexa.get(i).trim()));
			}
			else if (i==4)  {
				
				for(int j=4; j<messageLength; j++) {
					
					if(Integer.parseInt(listHexa.get(j).trim()) < 9 ) {
						if(i==4) {
							for (Map.Entry mapentry : mapURI.entrySet()) {
								//System.out.println(mapentry.getKey() );
								//System.out.println(listHexa.get(i));
								if(mapentry.getKey().equals(listHexa.get(i).trim())) {
									
									System.out.println(mapentry.getValue());
								}
							}
						}
					}
					
					System.out.println(hexToAscii(listHexa.get(j).trim()));
					message=message+hexToAscii(listHexa.get(j).trim());
				}
				i= i+messageLength;
			}
			
		}
			
	}

}
